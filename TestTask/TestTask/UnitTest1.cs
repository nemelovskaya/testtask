﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using OpenPop.Mime;
using OpenPop.Pop3;
using Assert = NUnit.Framework.Assert;

namespace TestTask
{
	[TestFixture]
	public class UnitTest1
	{
		private ChromeDriver chrome;

		[SetUp]
		public void Setup()
		{
			chrome = new ChromeDriver();

		}

		[Test]
		[TestCase("TaylorSmith", "testtaskdk144@gmail.com", "12345qwer")]
		public void CreateAcc(string name, string email, string pass)
		{
			chrome.Navigate().GoToUrl("http://www.anastasiadate.com");
			Thread.Sleep(3000);
			chrome.FindElement(By.XPath("//select[@id='im-a']//option[2]")).Click();
			Thread.Sleep(3000);
			chrome.FindElement(By.Id("name")).SendKeys(name);
			chrome.FindElement(By.XPath("//div[@class='form-content']/div[4]/div[1]/div[6]/input")).SendKeys(email);
			chrome.FindElement(By.Id("pass")).SendKeys(pass);
			Thread.Sleep(3000);
			chrome.FindElement(By.XPath("//div[@class='form-content']//button[.='Find Your Matches']")).Click();
			Thread.Sleep(9000);
			chrome.FindElement(By.CssSelector("span.my-profile")).Click();
			Thread.Sleep(3000);
			chrome.FindElement(By.LinkText("Log Out")).Click();
			Thread.Sleep(3000);
		}

		[Test]
		[TestCase("testtaskdk9@gmail.com", "12345qwer")]
		public void Login(string login, string pass)
		{
			chrome.Navigate().GoToUrl("http://www.anastasiadate.com");
			Thread.Sleep(3000);
			chrome.FindElement(By.CssSelector("span.button.default")).Click();
			Thread.Sleep(3000);
			chrome.FindElement(By.Name("email")).SendKeys(login);
			chrome.FindElement(By.Name("password")).SendKeys(pass);
			Thread.Sleep(3000);
			chrome.FindElement(By.XPath("//form[@id='member-login']//button[.='Sign In']")).Click();
			Thread.Sleep(9000);
			chrome.FindElement(By.CssSelector("span.my-profile")).Click();
			Thread.Sleep(3000);
			chrome.FindElement(By.LinkText("Log Out")).Click();
			Thread.Sleep(3000);
		}


		[Test]
		[TestCase("taylorsmithdk@gmail.com", "12345qwer")]
		public void GetMessages(string login, string pass)
		{
			//We don't use using directive because gmail will return nothing
			//in next time if we closing current connection
			Pop3Client client = new Pop3Client();
			client.Connect("pop.gmail.com", 995, true);

			client.Authenticate(login, pass);

			//client.Authenticate("taylorsmithdk@gmail.com", "12345qwer");

			// Get the number of messages in the inbox
			int messageCount = client.GetMessageCount();
			if (messageCount <= 0)
			{
				throw new Exception("Empty mail count");
			}
			// We want to download all messages
			List<Message> allMessages = new List<Message>(messageCount);

			// Messages are numbered in the interval: [1, messageCount]
			// Ergo: message numbers are 1-based.
			// Most servers give the latest message the highest number
			for (var i = messageCount; i > 0; i--)
			{
				allMessages.Add(client.GetMessage(i));
			}

			foreach (var msg in allMessages)
			{
				if (msg.Headers.Subject.Contains("Please confirm your email address") &&
					msg.Headers.From.DisplayName.Contains("AnastasiaDate Team"))
				{
					var html = GetTextFromMail(msg);
					var links = FindAllLinks(html);

					foreach (var a in links)
					{
						if (a.Contains("www.anastasiadate.com/emailconfirmation"))
						{
							var link = a.Replace("&amp;", "&");
							chrome.Navigate().GoToUrl(link);
							Thread.Sleep(3000);
							FindTextOnPage("Your email has been confirmed.");
						}
					}

					return;
				}
			}

			throw new Exception("Needed message didn't found");
		}

		private void FindTextOnPage(string text)
		{
			string bodyText = chrome.FindElement(By.TagName("body")).Text;
			Assert.IsTrue(bodyText.Contains(text));
		}

		private List<string> FindAllLinks(string html)
		{
			Regex reHref = new Regex(@"(?inx)
			<a \s [^>]*
			    href \s* = \s*
			        (?<q> ['""] )
			            (?<url> [^""]+ )
			        \k<q>
			[^>]* >");

			List<string> links = new List<string>();

			foreach (Match match in reHref.Matches(html))
				links.Add(match.Groups["url"].ToString());

			return links;
		}

		private string GetTextFromMail(Message msg)
		{
			OpenPop.Mime.MessagePart html = msg.FindFirstHtmlVersion();
			return html != null ? html.GetBodyAsText() : string.Empty;
		}

		[TearDown]
		public void TearDown()
		{
			if (chrome != null)
				chrome.Quit();
		}
	}
}
